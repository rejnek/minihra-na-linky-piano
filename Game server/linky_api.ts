const axios = require('axios');
const nconf = require('nconf');

class Comunicator {
    public static instance: Comunicator;

    private interval;
    private aToken: string;
    private debugMode: number;

    constructor(debugMode: number) {
        Comunicator.instance = this;
        this.debugMode = debugMode;
        nconf.file({ file: 'config.json' });
    }

    public CreateToken() {
        return axios.post('https://linky.fel.cvut.cz/Api/v1/Token/Create', {
            user_id: 'e811c1a8-d6a8-436f-9778-177f955a1b45',
            user_secret: '90b72d25-10fb-4b7b-841f-5af2240924aa',
            debug: this.debugMode,
            fps: 30
        }).then(res => {
            this.aToken = res.data.access_token;
            this.RefreshToken(res.data.refresh_token)
            setInterval(() => this.RefreshToken(res.data.refresh_token), 150 * 1000);

            console.log("CreateToken: " + res.status);
            console.log("aToken: " + this.aToken);
        }).catch(error => {
            console.error(error);
        })
    }

    private RefreshToken(token: string) {
        axios.post('https://linky.fel.cvut.cz/Api/v1/Token/Refresh', {
            refresh_token: token
        }).then(res => {
            console.log("RefreshToken: " + res.status)
        }).catch(error => {
            console.error(error)
        })
    }

    private Play(array: string[][][]) {
        axios.post(nconf.get('post_url'), {
            access_token: this.aToken,
            frames: array,
            clear_buffer: true
        }).then(res => {
            console.log("Play: " + res.status)
        }).catch(error => {
            console.error("Play: ERROR!!!");
        })

        // console.dir(array, {'maxArrayLength': null});
    }

    public PlayLinky(array: string[][], speed: number, refresh: number) {
        let time = 0;

        //posilej kazdych Xs novy obraz
        this.interval = setInterval(() => {
            time += refresh;
            this.PlaySection(array, time, speed);
        }, refresh * 1000);
    }

    public PlaySection(array: string[][], time: number, speed: number) {
            let columns = new Array(5);
            // pro kazdy sloupec
            for (let j = 0; j < columns.length; j += 1) {
                // test jestli nejsme na konci
                if (time * speed + 204 > array[j].length) {
                    columns[j] = new Array(204).fill('000000');
                } else {
                    let begin = (time * speed) | 0;
                    columns[j] = (array[j].slice(begin, begin + 204)).reverse();
                    if(columns[j].length != 204) console.log("Length Error PlaySection()")
                }
            }

        this.Play([columns]);
    }

    public Stop() {
        this.ClearScreen();
        clearInterval(this.interval);
    }

    public ClearScreen() {
        let a = new Array(1); // snimky
        for (let i = 0; i < a.length; i += 1) {
            a[i] = new Array(5); // sloupce
            for (let j = 0; j < a[i].length; j += 1) {
                a[i][j] = new Array(204).fill('000000'); // radky
            }
        }
        this.Play(a);
    }
}

module.exports = {Comunicator};


