class Game {
    public static instance: Game;

    public duration: number; // mobile readable
    public speed:number;
    public players: Player[];
    public mobileMap: Note[]; // mobile readable
    public map: any[];

    public constructor() {
        Game.instance = this;

        this.mobileMap = [];
        this.players = [];
    }

    public GenerateMap(speed:number, noteSize:number, gameLength: number){
        this.speed = speed;
        this.duration = gameLength;

        this.map = new Array(5); // sloupce
        for (let i = 0; i < this.map.length; i += 1) {
            this.map[i] = new Array(this.speed * gameLength); // radky
            this.map[i].fill('000000');
        }

        // Naplneni mapy

        let min = 0;
        let max = noteSize / 2;
        let block = 204;

        console.log(this.map[0].length);

        while (block + noteSize < this.map[0].length) {
            let key = Game.randomInteger(0, 4);

            this.map[key].fill('FF00FF', block, block + noteSize);
            this.mobileMap.push(new Note("k" + key, (block / this.speed) * 1000, ((block + noteSize) / this.speed) * 1000));
            block = block + noteSize + Game.randomInteger(min, max);

            console.log('FF00FF');
        }

        // konec naplnovani
        for (let i = 0; i < this.map.length; i += 1) {
            this.map[i] = this.map[i].concat(new Array(204).fill('000000'));
        }

        // console.log(this.map);
    }

    public RemovePlayer(idPlayer: string) {
        for (let i = 0; i < this.players.length; i += 1) {
            if (this.players[i].idPlayer === idPlayer) {
                console.log('removing: ' + this.players[i].idPlayer);
                this.players.splice(i, 1)
                return;
            }
        }
    }

    // float min (included) and max (not included):
    static randomNumber(min, max) {
        return Math.random() * (max - min) + min;
    }

    // int min (included) and max (included):
    static randomInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}

class Note {
    public code: string;
    public hitBegin: Number;
    public hitEnd: Number;

    public constructor(code: string, hitBegin: Number, hitEnd: Number) {
        this.code = code;
        this.hitBegin = hitBegin;
        this.hitEnd = hitEnd;
    }
}

class Player {
    public idPlayer: string;
    public nickname: string;
    public ready: boolean;
}

module.exports = {Game, Note};