function switchTab(thisTab, nextTab) {
    // pokud jsi jsme jisty tabem, tak staci tohle
    // document.getElementById(thisTab).style.display = "none";

    let windows = document.getElementsByClassName('w') as HTMLCollectionOf<HTMLElement>;
    for(let i = 0; i < windows.length; i += 1){
        windows[i].style.display = "none";
    }

    document.getElementById(nextTab).style.display = "flex";
}

function fullScreen(){
    // return;
    document.documentElement.requestFullscreen();
    window.screen.orientation.lock("landscape-primary").then(r => {});
}

function switchFullscreen() {
    if (document.fullscreenElement) {
        if (document.exitFullscreen()) {
            document.exitFullscreen();
        }
    } else {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        }
    }
}

function keyPressed(idKey:string){
    console.log(idKey + " " + Date.now());
}
