class GameMobile {
    public static instance: GameMobile;

    public timeRemaining: number;
    public score: number;
    public active: boolean;

    private duration: number;
    private startTime: number;
    private map: Note[];

    private txtTime: HTMLHeadingElement;
    private txtScore: HTMLHeadingElement;

    public constructor(txtTime: HTMLHeadingElement, txtScore: HTMLHeadingElement) {
        GameMobile.instance = this;
        this.active = false;

        this.score = 0;

        this.txtScore = txtScore;
        this.txtTime = txtTime;
    }

    public start(game: Game) {
        this.timeRemaining = game.duration * 1000;
        this.score = 0;
        this.active = true;

        this.duration = game.duration * 1000;
        this.startTime = Date.now();
        this.map = game.mobileMap;

        this.txtScore.textContent = this.score.toString();
        this.txtTime.textContent = Math.floor(this.timeRemaining / 1000) + "s";
    }

    public keyPress(code: string) {
        let tFromStart = Date.now() - this.startTime;
        for (const note of this.map) {
            if (note.hitBegin < tFromStart && note.hitEnd > tFromStart && note.code == code) {
                this.score += 10;
                this.txtScore.textContent = this.score.toString();
                return;
            }
        }
        this.score -= 10;
        this.txtScore.textContent = this.score.toString();
    }

    public async updateTime() {
        while (this.timeRemaining > 1000 && this.active) {
            await this.delay(1000);
            this.timeRemaining = this.duration - (Date.now() - this.startTime);
            this.txtTime.textContent = Math.floor(this.timeRemaining / 1000) + "s";
        }
    }

    async delay(milliseconds: number) {
        return new Promise<void>((resolve) => {
            setTimeout(resolve, milliseconds);
        });
    }
}

class Game {
    public duration: number;
    public mobileMap: Note[];
}

class Note {
    public code: string;
    public hitBegin: number;
    public hitEnd: number;
}