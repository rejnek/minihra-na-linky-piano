function switchTab(thisTab, nextTab) {
    // pokud jsi jsme jisty tabem, tak staci tohle
    // document.getElementById(thisTab).style.display = "none";
    var windows = document.getElementsByClassName('w');
    for (var i = 0; i < windows.length; i += 1) {
        windows[i].style.display = "none";
    }
    document.getElementById(nextTab).style.display = "flex";
}
function fullScreen() {
    // return;
    document.documentElement.requestFullscreen();
    window.screen.orientation.lock("landscape-primary").then(function (r) { });
}
function switchFullscreen() {
    if (document.fullscreenElement) {
        if (document.exitFullscreen()) {
            document.exitFullscreen();
        }
    }
    else {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        }
    }
}
function keyPressed(idKey) {
    console.log(idKey + " " + Date.now());
}
