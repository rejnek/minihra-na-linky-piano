class Lobby {
    public static instance: Lobby;

    private queue: Player[];

    constructor() {
        Lobby.instance = this;

        this.queue = [];
    }

    public ClearLobby(){
        this.queue = [];
    }

    public AddPlayer(newPlayer: Player) {
        for (let i = 0; i < this.queue.length; i += 1) {
            if (this.queue[i].idPlayer === newPlayer.idPlayer) {
                let oldReady = this.queue[i].ready;
                this.queue[i] = newPlayer;
                this.queue[i].ready = oldReady;
                return;
            }
        }
        this.queue.push(newPlayer);
    }

    public RemovePlayer(idPlayer: string) {
        for (let i = 0; i < this.queue.length; i += 1) {
            if (this.queue[i].idPlayer === idPlayer) {
                console.log('removing: ' + this.queue[i].idPlayer);
                this.queue.splice(i, 1)
                return;
            }
        }
    }

    public ReadyPlayer(idPlayer: string) {
        for (const player of this.queue) {
            if (player.idPlayer === idPlayer) {
                player.ready = true;
                return;
            }
        }
    }

    public EverybodyReady(): boolean {
        let count = 0;
        for(const player of this.queue) {
            if (player.ready === true) {
                count += 1;
            }
        }
        return (count >= 5 || count == this.queue.length) && count != 0;
    }

    public QueueArray() {
        let result = [];
        for (const player of this.queue) {
            if (player.ready) {
                result.push([player.nickname, "ready"]);
            } else {
                result.push([player.nickname, "waiting"]);
            }
        }
        return result;
    }
}

class Player {
    public idPlayer: string;
    public nickname: string;
    public ready: boolean;

    constructor(idPlayer: string, nickname: string) {
        this.idPlayer = idPlayer;
        this.nickname = nickname;
        this.ready = false;
    }
}

module.exports = {Lobby, Player};
