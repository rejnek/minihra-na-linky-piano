// -- test kresleni
// import {Print_frame} from "./print_frame";
// Print_frame.Run();
// --

const path = require('path')
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const {Lobby, Player} = require("./lobby_server.js");
const {Comunicator} = require("./linky_api");
const {Game} = require("./game_server.js");

const port = process.env.PORT || 3010;
app.use(express.static(__dirname + '/Web app'));

// nastaveni promenych
const nconf = require('nconf');
nconf.file({ file: 'config.json' });

const debugMode:number = nconf.get('debug_mode');
const gameDuration:number = nconf.get('duration');
const gameSpeed:number = nconf.get('speed');
const noteSize:number = nconf.get('note_size');
const screenRefresh:number = nconf.get('refresh');



new Lobby();
new Game();
new Comunicator(debugMode);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/Web app/index.html');
});

io.on('connection', (socket) => {
    console.log('user connected: ' + socket.id)

    socket.on('login', msg => {
        Lobby.instance.AddPlayer(new Player(socket.id, msg.toString()));
        io.emit('queue', Lobby.instance.QueueArray());
        testReady()

        // console.log('login: ' + socket.id);
    });

    socket.on('signOut', () => {
        Lobby.instance.RemovePlayer(socket.id);
        Game.instance.RemovePlayer(socket.id);

        io.emit('queue', Lobby.instance.QueueArray());
        testReady()
    });

    socket.on('ready', msg => {
        Lobby.instance.ReadyPlayer(socket.id);
        io.emit('queue', Lobby.instance.QueueArray());
        testReady()

        console.log(Lobby.instance.queue);
    });

    socket.on('disconnect', () => {
        Lobby.instance.RemovePlayer(socket.id);
        Game.instance.RemovePlayer(socket.id);

        console.log(socket.id + " disconnected")
        io.emit('queue', Lobby.instance.QueueArray());
        testReady()
    });

    socket.on('score', (msg) => {
        for (let player of Game.instance.players) {
            io.to(player.idPlayer).emit('scoreBoardEntry', msg);
        }
    });

    function testReady() {
        if (Lobby.instance.EverybodyReady() && Game.instance.players.length == 0) {
            Game.instance.GenerateMap(gameSpeed, noteSize, gameDuration);
            Game.instance.players = Array.from(Lobby.instance.queue);

            for (let player of Lobby.instance.queue) {
                io.to(player.idPlayer).emit('startGame', Game.instance);
            }

            Lobby.instance.ClearLobby();
            endGame(gameDuration * 1000).then(r => null);

            // posilani obrazu na Linky -------
            Comunicator.instance.CreateToken()
                .then(() =>
                    Comunicator.instance.PlayLinky(Game.instance.map, Game.instance.speed, screenRefresh)
                );
            // --------------------------------
        }
        else {
            Comunicator.instance.Stop();
        }
    }

    async function endGame(duration: number) {
        await new Promise<void>((resolve) => {
            setTimeout(resolve, duration);
        });
        for (let player of Game.instance.players) {
            io.to(player.idPlayer).emit('endGame');
        }
        Comunicator.instance.Stop();

        setTimeout(function(){
            Game.instance.players = [];
        }, 3000); // score musi byt odeslane do 3s
    }
});

http.listen(port, () => {
    console.log(`Socket.IO server running at http://localhost:${port}/`);
});